import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../service/api.service';

@Component({
  selector: 'app-watch-list',
  templateUrl: './watch-list.component.html',
  styleUrls: ['./watch-list.component.css']
})
export class WatchListComponent implements OnInit {

  Watch: any = [];
  loading_message = ""
  error_loading = false
  constructor(private apiService: ApiService) {
    this.readWatch();
  }
  ngOnInit() { }
  readWatch() {
    this.loading_message = "Loading watches catalog..."
    this.error_loading = false   
    this.apiService.getWatches().subscribe(
      data => {
        this.Watch = data;
      },
      error => {
        this.loading_message = ""
        this.error_loading = true
      },)
  }

}
